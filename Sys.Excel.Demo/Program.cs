﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using Sys.Excel.Aspose;

namespace Sys.Excel.Demo
{
    class Program
    {
        static int count = 0;
        private static Random ran = new Random();

        private static Color RandColor()
        {
            return Color.FromArgb(ran.Next(0, 255), ran.Next(0, 255), ran.Next(0, 255));
        }

        private static IEnumerable<SysUser> Get_Users()
        {
            for (int i = 0; i < 3000; i++)
            {
                count++;
                yield return new SysUser()
                {
                    SysUserOID = Guid.NewGuid(),
                    CreateUserOID = Guid.NewGuid().ToString(),
                    LoginName = "admin_" + i,
                    PassWord = "123456",
                    PersonName = "系统管理员_" + i,
                    Sex = i % 2 == 0 ? "男" : "女",
                    Email = i.GetHashCode() + "@qq.com",
                    PhoneNumber = i.GetHashCode().GetHashCode().ToString(),
                    UpdateTime = DateTime.Now
                };
            }
        }

        static void Main(string[] args)
        {
            var ps = typeof(SysUser).GetProperties();

            Settings sets = new Settings()
            {
                PageSize = 555,
                Name = "系统用户",
                ColumnsAutoWidth = true,
                StartColIndex = 1,
                StartRowIndex = 1,
                DataSource = new Func<IEnumerable<SysUser>>(Get_Users),
                FreezeHeader = true,
                //RowStyleGetter = (i, row) => i % 2 == 0 ? CellStyle.Body : new CellStyle() { BgColor = Color.White, FgColor = RandColor() },
                RootNodes = new[]{
                    new ColNode(){
                        Title = "系统用户列表",
                        ChildColNodes = new []{
                            new ColNode(){
                                Title = "含有ID的列",
                                ChildColNodes = ps.Where(p => p.Name.EndsWith("ID")).Select(p => new ColNode(){
                                        Field = p.Name,
                                        Title = p.Name + "_的标题"
                                    }).ToArray()
                            },
                            new ColNode(){
                                Title = "不含ID的列",
                                ChildColNodes = ps.Where(p => !p.Name.EndsWith("ID")).Select(p => new ColNode(){
                                        Field = p.Name,
                                        Title = p.Name + "_的标题"
                                    }).ToArray()
                            }
                        }
                    }
                }
            };

            IExcelExporter exporter = new Aspose_Cells_ExcelExporter();

            var watch = Stopwatch.StartNew();

            exporter.BuildSheetsAsync(
                new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Users.xlsx"), FileMode.Create, FileAccess.Write),
                false, sets).Wait();

            watch.Stop();

            Console.WriteLine(string.Format("共耗时：{0} 毫秒", watch.ElapsedMilliseconds));
            Console.WriteLine("Press any key to exit ...");
            Console.ReadKey();
        }


        public class SysUser
        {
            public SysUser() { }

            public Guid SysUserOID { get; set; }
            public string CreateUserOID { get; set; }
            public string LoginName { get; set; }
            public string PassWord { get; set; }
            public string PersonName { get; set; }
            public string Sex { get; set; }
            public string Email { get; set; }
            public string PhoneNumber { get; set; }
            public DateTime? UpdateTime { get; set; }
        }
    }
}
