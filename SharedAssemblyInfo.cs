﻿using System.Reflection;
using System.Security;

[assembly: AssemblyTitle("Sys")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("CXL-1989")]
[assembly: AssemblyProduct("Sys")]
[assembly: AssemblyCopyright("Copyright © CXL-1989")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyInformationalVersion("1.0.0.0")]
[assembly: AllowPartiallyTrustedCallers]
[assembly: SecurityRules(SecurityRuleSet.Level1)]