﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;

namespace Sys.Excel
{
    #region [ CellFormatter ]

    public delegate string CellFormatter(int i, int j, object row, object value);

    #endregion

    #region [ CellStyle ]

    [Serializable]
    public sealed class CellStyle
    {
        private int m_code = 0;
        private string m_str = null;
        public static readonly CellStyle Body = new CellStyle(Color.Black, Color.White);
        public static readonly CellStyle Header = new CellStyle(Color.Black, Color.FromArgb(192, 192, 192), Align.Center, FontBoldWeight.Bold);

        public enum Align { Left, Center, Right }

        public enum FontBoldWeight { Normal, Bold }

        private bool Color_EQ(Color a, Color b)
        {
            return a.A == b.A && a.R == b.R && a.G == b.G && a.B == b.B;
        }

        private string Color_RGBA(Color color)
        {
            return string.Format("RGBA({0}, {1}, {2}, {3})", color.R, color.G, color.B, color.A);
        }

        public CellStyle()
            : this(Color.Black, Color.White, Align.Left, FontBoldWeight.Normal)
        {

        }

        public CellStyle(
            Color fg_color, Color bg_color,
            Align align = Align.Left, FontBoldWeight weight = FontBoldWeight.Normal)
        {
            this.FgColor = fg_color;
            this.BgColor = bg_color;
            this.TextAlign = align;
            this.FontWeight = weight;

            this.m_str = string.Format(
                "FgColor: {0}; BgColor: {1}; TextAlign: {2}, FontWeight: {3};",
                this.Color_RGBA(this.FgColor), this.Color_RGBA(this.BgColor), this.TextAlign, this.FontWeight);
            this.m_code = this.m_str.GetHashCode();
        }

        public Color FgColor { get; set; }

        public Color BgColor { get; set; }

        public Align TextAlign { get; set; }

        public FontBoldWeight FontWeight { get; set; }

        public override bool Equals(object obj)
        {
            CellStyle style = obj as CellStyle;
            return style != null &&
                this.Color_EQ(this.FgColor, style.FgColor) &&
                this.Color_EQ(this.BgColor, style.BgColor) &&
                this.TextAlign == style.TextAlign &&
                this.FontWeight == style.FontWeight;
        }

        public override int GetHashCode() { return this.m_code; }

        public override string ToString() { return this.m_str; }
    }

    #endregion

    #region [ Settings ]

    [Serializable]
    public sealed class Settings
    {
        private int m_maxlevel = 0;
        private IList<ColNode> m_leafNodes = null;
        internal static readonly IDictionary<string, CellFormatter> m_dictFormatters = new Dictionary<string, CellFormatter>();

        public Settings()
        {
            this.ColumnsAutoWidth = true;
        }

        /// <summary>
        /// 注册 CellFormatter
        /// </summary>
        /// <param name="name"></param>
        /// <param name="formatter"></param>
        public static void RegCellFormatter(string name,
            CellFormatter formatter) { lock (Settings.m_dictFormatters) { Settings.m_dictFormatters[name] = formatter; } }

        /// <summary>
        /// GetLeafNodes
        /// </summary>
        /// <param name="p_nodes"></param>
        /// <returns></returns>
        private IList<ColNode> GetLeafNodes(IList<ColNode> p_nodes)
        {
            IList<ColNode> leafNodes = new List<ColNode>();
            if (p_nodes != null && p_nodes.Count > 0)
            {
                for (int i = 0; i < p_nodes.Count; i++)
                {
                    if (p_nodes[i] == null)
                        p_nodes[i] = new ColNode() { };

                    ColNode p_node = p_nodes[i];

                    if (p_node.ChildColNodes == null ||
                        p_node.ChildColNodes.Count == 0) { leafNodes.Add(p_node); continue; }

                    foreach (ColNode node in
                        p_node.ChildColNodes) { node.Level_Index = p_node.Level_Index + 1; }

                    foreach (ColNode node in
                        this.GetLeafNodes(p_node.ChildColNodes)) { leafNodes.Add(node); }
                }
            }
            return leafNodes;
        }

        /// <summary>
        /// 最大级数
        /// </summary>
        internal int MaxLevel
        {
            get
            {
                return this.m_maxlevel == 0 ?
                    this.LeafNodes.Max(node => node.Level_Index) : this.m_maxlevel;
            }
        }

        /// <summary>
        /// 所有叶
        /// </summary>
        public IList<ColNode> LeafNodes
        {
            get
            {
                this.m_leafNodes =
                    this.m_leafNodes ?? this.GetLeafNodes(this.RootNodes);
                return this.m_leafNodes;
            }
        }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 每页数量(当 PageSize == 0 时不分页，默认为 0)
        /// </summary>
        public uint PageSize { get; set; }

        /// <summary>
        /// Excel 起始行
        /// </summary>
        public uint StartRowIndex { get; set; }

        /// <summary>
        /// Excel 起始列
        /// </summary>
        public uint StartColIndex { get; set; }

        /// <summary>
        /// 是否冻结表头
        /// </summary>
        public bool FreezeHeader { get; set; }

        /// <summary>
        /// 列宽自动
        /// </summary>
        public bool ColumnsAutoWidth { get; set; }

        /// <summary>
        /// 表头单元格样式
        /// </summary>
        public CellStyle HeaderCellStyle { get; set; }

        /// <summary>
        /// 数据源
        /// </summary>
        public object DataSource { get; set; }

        /// <summary>
        /// 定义如何获取单元格数据
        /// </summary>
        public Func<object, string, object> CellValueGetter { get; set; }

        /// <summary>
        /// 获取行样式
        /// </summary>
        public Func<int, object, CellStyle> RowStyleGetter { get; set; }

        /// <summary>
        /// 获取列样式
        /// </summary>
        public Func<int, ColNode, CellStyle> ColumnStyleGetter { get; set; }

        /// <summary>
        /// 根级
        /// </summary>
        public IList<ColNode> RootNodes { get; set; }
    }

    #endregion

    #region [ ColNode ]

    [Serializable]
    public sealed class ColNode
    {
        private List<ColNode> m_leaf_nodes = null;
        private IList<ColNode> m_child_nodes = null;

        public ColNode()
        {
            this.Width = 30;
        }

        /// <summary>
        /// 节点所处的级别
        /// </summary>
        internal int Level_Index { get; set; }

        /// <summary>
        /// 叶节点
        /// </summary>
        internal IList<ColNode> LeafNodes
        {
            get
            {
                if (this.m_leaf_nodes == null)
                {
                    this.m_leaf_nodes = new List<ColNode>();
                    if (this.ChildColNodes != null && this.ChildColNodes.Count > 0)
                    {
                        foreach (ColNode node in this.ChildColNodes)
                        {
                            if (node.ChildColNodes == null || node.ChildColNodes.Count == 0) { this.m_leaf_nodes.Add(node); continue; }
                            this.m_leaf_nodes.AddRange(node.LeafNodes);
                        }
                    }
                    else { this.m_leaf_nodes.Add(this); }
                }
                return this.m_leaf_nodes;
            }
        }

        /// <summary>
        /// 字段
        /// </summary>
        public string Field { get; set; }

        /// <summary>
        /// 合并列中值相同的单元格用做对比的字段
        /// </summary>
        public string MergeField { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 列宽
        /// </summary>
        public uint Width { get; set; }

        /// <summary>
        /// 格式化
        /// </summary>
        public CellFormatter Formatter { get; set; }

        /// <summary>
        /// 格式化器的名称
        /// </summary>
        public string FormatterName { get; set; }

        /// <summary>
        /// 子级
        /// </summary>
        public IList<ColNode> ChildColNodes
        {
            get { return this.m_child_nodes; }
            set { this.m_leaf_nodes = null; this.m_child_nodes = value; }
        }
    }

    #endregion

    #region [ IExcelExporter ]

    public interface IExcelExporter
    {
        Task BuildSheetsAsync(
            Stream stream, IEnumerable<Settings> settings_list, bool useOldVersion = false,
            object hostContext = null);
    }

    #endregion

    #region [ IExcelExporter_Extensions ]

    public static class IExcelExporter_Extensions
    {
        public static Task BuildSheetsAsync(
            this IExcelExporter exporter, Stream stream, bool useOldVersion,
            params Settings[] settings_array) { return exporter.BuildSheetsAsync(stream, settings_array, useOldVersion); }

        public static Task BuildSheetsAsync_2003(
            IExcelExporter exporter, Stream stream,
            params Settings[] settings_array) { return exporter.BuildSheetsAsync(stream, settings_array, true); }

        public static Task BuildSheetsAsync_2007(
            IExcelExporter exporter, Stream stream,
            params Settings[] settings_array) { return exporter.BuildSheetsAsync(stream, settings_array); }
    }

    #endregion

    #region [ ExcelExporterBase<TWorkBook, TWorkSheet, TCellStyle> ]

    public abstract class ExcelExporterBase<TWorkBook, TWorkSheet, TCellStyle> : IExcelExporter
        where TWorkBook : class
        where TWorkSheet : class
        where TCellStyle : class
    {

        #region [ private Private_GetCellStyle ]

        private TCellStyle Private_GetCellStyle(
            TWorkBook workbook, IDictionary<CellStyle, TCellStyle> stylesDict,
            TCellStyle style, CellStyle cellStyle)
        {
            TCellStyle t_style = null;
            if (!stylesDict.TryGetValue(cellStyle, out t_style))
            {
                t_style = this.CreateCellStyle(workbook, style, cellStyle);
                stylesDict[cellStyle] = t_style;
                return t_style;
            }
            return stylesDict[cellStyle];
        }

        #endregion

        #region [ private Get_ValueGetter ]

        private Func<object, string, object> Get_ValueGetter(Settings settings)
        {
            return settings.CellValueGetter ?? new Func<object, string, object>(
                (obj, field) =>
                {
                    if (obj != null && field != null)
                    {
                        if (obj is DataRow)
                        {
                            DataRow row = obj as DataRow;
                            if (row.Table.Columns.Contains(field)) return row[field];
                        }
                        else if (obj is IDictionary)
                        {
                            IDictionary dict = obj as IDictionary;
                            if (dict.Contains(field)) return dict[field];
                        }
                        else if (obj is IDictionary<string, object>)
                        {
                            IDictionary<string, object> dict_g = obj as IDictionary<string, object>;
                            if (dict_g.ContainsKey(field)) return dict_g[field];
                        }
                        else
                        {
                            Type obj_type = obj.GetType();
                            PropertyInfo pro = obj_type.GetProperty(field, BindingFlags.Public | BindingFlags.Instance);
                            if (pro != null) return pro.GetValue(obj, null);
                            FieldInfo fie = obj_type.GetField(field, BindingFlags.Public | BindingFlags.Instance);
                            if (fie != null) return fie.GetValue(obj);
                        }
                    }
                    return null;
                });
        }

        #endregion

        #region [ private Get_DataSource ]

        private IEnumerable<object> Get_DataSource(Settings settings)
        {
            IEnumerable<object> data = null;
            object dataSource = settings.DataSource;

            if (dataSource != null)
            {
                Type type = dataSource.GetType();
                if (type.IsGenericType)
                {
                    Type g_type = type.GetGenericTypeDefinition();
                    if (g_type == typeof(Func<>))
                    {
                        dataSource = ((Delegate)dataSource).DynamicInvoke();
                    }
                    else if (g_type == typeof(Lazy<>))
                    {
                        dataSource = type.GetProperty("Value").GetValue(dataSource, null);
                    }
                }

                if (dataSource is DataSet)
                {
                    DataSet dataSet = dataSource as DataSet;
                    dataSource = dataSet.Tables.Count > 0 ? dataSet.Tables[0] : null;
                }

                if (dataSource is DataTable)
                {
                    data = (dataSource as DataTable).AsEnumerable().Cast<object>();
                }
                else if (dataSource is IEnumerable)
                {
                    data = (dataSource as IEnumerable).Cast<object>();
                }
            }

            return data;
        }

        #endregion

        #region [ private ToPagerSettings ]

        private IEnumerable<Settings> ToPagerSettings(Settings settings)
        {
            if (settings != null)
            {
                if (settings.DataSource == null ||
                    settings.PageSize == 0) yield return settings;
                else
                {
                    int index = 0;
                    var dataSource = this.Get_DataSource(settings);
                    if (dataSource == null) yield return settings;
                    else
                    {
                        while (true)
                        {
                            var pager = dataSource.Skip(
                                index * (int)settings.PageSize).Take((int)settings.PageSize);
                            if (!pager.Any()) { yield break; }
                            index++;
                            yield return new Settings()
                            {
                                CellValueGetter = settings.CellValueGetter,
                                ColumnStyleGetter = settings.ColumnStyleGetter,
                                ColumnsAutoWidth = settings.ColumnsAutoWidth,
                                DataSource = pager,
                                FreezeHeader = settings.FreezeHeader,
                                HeaderCellStyle = settings.HeaderCellStyle,
                                Name = string.Format("{0}({1})", settings.Name, index),
                                PageSize = 0,
                                RootNodes = settings.RootNodes,
                                RowStyleGetter = settings.RowStyleGetter,
                                StartColIndex = settings.StartColIndex,
                                StartRowIndex = settings.StartRowIndex
                            };
                        }
                    }
                }
            }
        }

        #endregion

        #region [ private BuildHeader ]

        private void BuildHeader(
            TWorkBook workbook, TWorkSheet sheet,
            Settings settings, IList<ColNode> p_nodes, IDictionary<CellStyle, TCellStyle> stylesDict,
            int startRowIndex = 0, int startColIndex = 0)
        {
            if (p_nodes != null && p_nodes.Count > 0)
            {
                for (int i = 0; i < p_nodes.Count; i++)
                {
                    ColNode p_node = p_nodes[i];
                    int col_index = startColIndex;
                    int row_index = startRowIndex + p_node.Level_Index;
                    bool is_leaf = p_node.ChildColNodes == null || p_node.ChildColNodes.Count == 0;

                    this.SetCell(
                        workbook, sheet,
                        row_index, col_index, settings, p_node, p_node.Title,
                        default(TCellStyle), true, false);

                    if (!is_leaf)
                    {
                        this.BuildHeader(
                            workbook, sheet, settings, p_node.ChildColNodes,
                            stylesDict, startRowIndex, col_index);
                    }

                    startColIndex += p_node.LeafNodes.Count;

                    this.MergeCells(
                        workbook, sheet, row_index,
                        is_leaf ? startRowIndex + settings.MaxLevel : row_index, col_index,
                        startColIndex - 1, settings);
                }
            }
        }

        #endregion

        #region [ private SetHeaderStyle ]

        private void SetHeaderStyle(
            TWorkBook workbook, TWorkSheet sheet,
            Settings settings, IList<ColNode> p_nodes, IDictionary<CellStyle, TCellStyle> stylesDict,
            int startRow, int startCol)
        {
            var style = this.Private_GetCellStyle(
                workbook, stylesDict, null, settings.HeaderCellStyle ?? CellStyle.Header);

            for (int i = 0; i < settings.MaxLevel + 1; i++)
            {
                for (int j = 0; j < settings.LeafNodes.Count; j++)
                {
                    this.SetCell(
                        workbook, sheet,
                        startRow + i, startCol + j, settings, null, null,
                        style, false);
                }
            }
        }

        #endregion

        #region [ private BuildBody ]

        private void BuildBody(
            TWorkBook workbook, TWorkSheet sheet,
            Settings settings, Tuple<IEnumerable<object>, Func<object, string, object>> tuple,
            IDictionary<CellStyle, TCellStyle> stylesDict,
            int startRowIndex, int startColIndex)
        {
            int i = 0;
            if (tuple != null)
            {
                IEnumerable<object> dataList = tuple.Item1;
                if (dataList != null)
                {
                    CellFormatter[] formatter_Array =
                        settings.LeafNodes.Select(node =>
                        {
                            if (node == null) return null;
                            CellFormatter formatter = node.Formatter;
                            if (formatter == null && node.FormatterName != null
                                ) { Settings.m_dictFormatters.TryGetValue(node.FormatterName, out formatter); }
                            return formatter;
                        }).ToArray();

                    CellStyle[] columns_Styles = settings.LeafNodes.Select(
                        (node, j) => settings.ColumnStyleGetter == null ? null : settings.ColumnStyleGetter(j, node)).ToArray();

                    foreach (object obj in dataList)
                    {
                        if (obj != null)
                        {
                            CellStyle row_style =
                                settings.RowStyleGetter == null ?
                                    null : settings.RowStyleGetter.Invoke(i, obj);

                            for (int j = 0, n_len = settings.LeafNodes.Count; j < n_len; j++)
                            {
                                ColNode node = settings.LeafNodes[j];
                                if (node != null)
                                {
                                    CellFormatter formatter = formatter_Array[j];
                                    object cell_val = tuple.Item2.Invoke(obj, node.Field);
                                    TCellStyle style = this.GetCellStyle(workbook, sheet, startRowIndex + i, startColIndex + j);

                                    this.SetCell(workbook, sheet,
                                        startRowIndex + i, startColIndex + j, settings,
                                        node, formatter == null ? cell_val : formatter.Invoke(i, j, obj, cell_val),
                                        this.Private_GetCellStyle(workbook, stylesDict, style, row_style ?? columns_Styles[j] ?? CellStyle.Body),
                                        cell_val != null);
                                }
                            }
                        }
                        i++;
                    }
                }
            }
        }

        #endregion

        #region [ private MergeBodyCells ]

        private void MergeBodyCells(
            TWorkBook workbook, TWorkSheet sheet,
            Settings settings, Tuple<IEnumerable<object>, Func<object, string, object>> tuple,
            int startRow, int startCol)
        {
            if (tuple != null && tuple.Item1 != null)
            {
                for (int i = 0, n_len = settings.LeafNodes.Count; i < n_len; i++)
                {
                    ColNode node = settings.LeafNodes[i];
                    if (node != null && !string.IsNullOrWhiteSpace(node.MergeField))
                    {
                        var listStrs = tuple.Item1.Select(
                            obj => Convert.ToString(tuple.Item2.Invoke(obj, node.MergeField))).ToList();

                        int count = 0;
                        int first_i = 0;
                        string first_val = null;
                        int list_count = listStrs.Count;
                        for (int j = 0; j < list_count + 1; j++)
                        {
                            string str_j = null;
                            if (j == list_count || (str_j = listStrs[j]) != first_val)
                            {
                                if (count > 0)
                                {
                                    this.MergeCells(
                                        workbook, sheet,
                                        startRow + first_i, startRow + first_i + count,
                                        startCol + i, startCol + i, settings);
                                }
                                count = 0; first_i = j; first_val = str_j;
                            }
                            else { count++; }
                        }
                    }
                }
            }
        }

        #endregion

        #region [ protected ]

        protected abstract TWorkBook CreateWorkBook(bool useOldVersion);

        protected abstract TWorkSheet CreateWorkSheet(TWorkBook workbook, string name, Settings settings);

        protected abstract TCellStyle CreateCellStyle(TWorkBook workbook, TCellStyle style, CellStyle cellStyle);

        protected abstract TCellStyle GetCellStyle(TWorkBook workbook, TWorkSheet sheet, int row, int col);

        protected abstract void SetCell(TWorkBook workbook, TWorkSheet sheet, int row, int col, Settings settings, ColNode node, object value, TCellStyle style, bool setValue = true, bool setStyle = true);

        protected abstract void MergeCells(TWorkBook workbook, TWorkSheet sheet, int startRow, int endRow, int startCol, int endCol, Settings settings);

        protected abstract void FreezePane(TWorkBook workbook, TWorkSheet sheet, int rowSplit, int colSplit);

        protected abstract void SetColumnsWidth(TWorkBook workbook, TWorkSheet sheet, Settings settings, uint[] widthArray, int startCol);

        protected abstract void AutoFillColumns(TWorkBook workbook, TWorkSheet sheet, Settings settings, int startCol);

        protected abstract void Save(TWorkBook workbook, bool useOldVersion, Stream stream);

        #endregion

        #region [ IExcelExporter ]

        public Task BuildSheetsAsync(
            Stream stream, IEnumerable<Settings> settings_list, bool useOldVersion = false,
            object hostContext = null)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            if (settings_list == null)
                throw new ArgumentNullException("settings_list");

            hostContext = hostContext ?? CallContext.HostContext;

            return Task.Factory.StartNew(() =>
            {
                TWorkBook workbook = this.CreateWorkBook(useOldVersion);
                IDictionary<CellStyle, TCellStyle> stylesDict = new Dictionary<CellStyle, TCellStyle>();

                if (CallContext.HostContext == null) { CallContext.HostContext = hostContext; }

                foreach (Settings settings in
                    settings_list.SelectMany(set => this.ToPagerSettings(set)))
                {
                    if (settings != null)
                    {
                        int len = settings.LeafNodes.Count;
                        int start_row = (int)settings.StartRowIndex;
                        int start_col = (int)settings.StartColIndex;
                        var dataSource = this.Get_DataSource(settings);
                        var valueGetter = this.Get_ValueGetter(settings);
                        var tuple = Tuple.Create(dataSource, valueGetter);
                        TWorkSheet sheet = this.CreateWorkSheet(workbook,
                            string.IsNullOrWhiteSpace(settings.Name) ? null : settings.Name, settings);

                        this.BuildHeader(
                            workbook, sheet, settings,
                            settings.RootNodes, stylesDict, start_row, start_col);

                        this.SetHeaderStyle(
                            workbook, sheet, settings,
                            settings.RootNodes, stylesDict, start_row, start_col);

                        this.BuildBody(
                            workbook, sheet, settings, tuple,
                            stylesDict, start_row + settings.MaxLevel + 1, start_col);

                        this.MergeBodyCells(
                            workbook, sheet, settings,
                            tuple, start_row + settings.MaxLevel + 1, start_col);

                        if (settings.FreezeHeader)
                            this.FreezePane(workbook, sheet, start_row + settings.MaxLevel + 1, start_col);

                        if (settings.ColumnsAutoWidth)
                        {
                            this.AutoFillColumns(workbook, sheet, settings, start_col);
                        }
                        else
                        {
                            this.SetColumnsWidth(workbook, sheet, settings,
                                settings.LeafNodes.Select(col => col.Width).ToArray(), start_col);
                        }
                    }
                }

                stylesDict.Clear();

                using (stream) { this.Save(workbook, useOldVersion, stream); }
            });
        }

        #endregion
    }

    #endregion
}